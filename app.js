"use strict"

const express = require('express')
const logger = require('morgan')
const path = require('path')
const favicon = require('serve-favicon')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const compress = require('compression')
const methodOverride = require('method-override')

const config = require('./config')

const app = express()

app.use(logger(config.logType))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

// view engine setup
app.set('view engine', 'pug')

app.use(cookieParser())
app.use(compress())
app.use(methodOverride())

app.use(favicon(path.join(__dirname, 'public', '/img/favicon.ico')))
app.use(express.static(path.join(__dirname, 'public')))
app.use('/bower', express.static(path.join(__dirname, 'bower_components')))

app.disable('x-powered-by')

app.use('/api', require('./routes/api'))
app.use('/template', require('./routes/template'))
app.use('/', require('./routes/index'))

// catch 404 and forward to error handler
app.use((req, res, next) => {

  res.status(404).end()

})

// error handler
// no stack-traces leaked to user in production
app.use((err, req, res, next) => {

  res.status(500).send({
    message: err.msg,
    error: config.env === 'development' ? err : {}
  })

})

module.exports = app