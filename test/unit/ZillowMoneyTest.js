"use strict"

const chai = require('chai')

const ZillowMoney = require('./../../src/ZillowMoney')

const should = chai.should()
const expect = chai.expect
const assert = chai.assert

/** ZillowMoneyTest */
describe("ZillowMoneyTest", function () {

  describe("constructor()", function () {

    it("Should create a money object if no args are passed in.", function (done) {

      const money = new ZillowMoney()
      expect(money).to.be.an.instanceOf(ZillowMoney)
      done()

    })

    it("Should create a money object if all args are passed.", function (done) {

      const money = new ZillowMoney({
        currency: "USD",
        amount: "23432432432"
      })
      expect(money).to.be.an.instanceOf(ZillowMoney)
      done()

    })

  })

  describe("setCurrency() and getCurrency()", function () {

    it("Should still work if no args are passed.", function (done) {

      const money = new ZillowMoney()
      money.setCurrency()
      assert.equal(money.getCurrency(), undefined)
      done()

    })


    it("Should set currency properly if currency is passed.", function (done) {

      const money = new ZillowMoney()
      money.setCurrency("AUD")
      assert.equal(money.getCurrency(), "AUD")
      done()

    })

  })

  describe("setAmount() and getAmount()", function () {

    it("Should default to 0 if no args are passed.", function (done) {

      const money = new ZillowMoney()
      money.setAmount()
      assert.equal(money.getAmount(), 0)
      done()

    })

    it("Should set amount properly if amount(number) is passed.", function (done) {

      const money = new ZillowMoney()
      money.setAmount(50000)
      assert.equal(money.getAmount(), 50000)
      done()

    })

    it("Should set amount properly if amount(string) is passed.", function (done) {

      const money = new ZillowMoney()
      money.setAmount("50000")
      assert.equal(money.getAmount(), 50000)
      done()

    })

  })

  describe("getMoneyFormat()", function () {

    it("Should return the amount of money in string format.", function (done) {

      const moneyComp = new ZillowMoney({
        currency: "USD",
        amount: "20000"
      })
      expect(moneyComp.toObject()).to.be.an("object")
      assert.equal(moneyComp.getMoneyFormat(), "$20,000")
      done()

    })

  })

  describe("getChangeFormat()", function () {

    it("Should the + or - change in currency amount(positive).", function (done) {

      const moneyComp = new ZillowMoney({
        currency: "USD",
        amount: "20000"
      })
      expect(moneyComp.toObject()).to.be.an("object")
      assert.equal(moneyComp.getChangeFormat(), "+$20,000")
      done()

    })

    it("Should the + or - change in currency amount(negative).", function (done) {

      const moneyComp = new ZillowMoney({
        currency: "USD",
        amount: "-20000"
      })
      expect(moneyComp.toObject()).to.be.an("object")
      assert.equal(moneyComp.getChangeFormat(), "-$20,000")
      done()

    })

  })

  describe("toObject()", function () {

    it("Should return object representation of money.", function (done) {

      const moneyComp = new ZillowMoney({
        currency: "USD",
        amount: "20000"
      })
      expect(moneyComp.toObject()).to.be.an("object")
      assert.equal(moneyComp.toObject().currency, "USD")
      assert.equal(moneyComp.toObject().amount, 20000)
      done()

    })

  })

})