"use strict"

const chai = require('chai')

const ZillowAddress = require('./../../src/ZillowAddress')

const should = chai.should()
const expect = chai.expect
const assert = chai.assert

/** ZillowAddressTest */
describe("ZillowAddressTest", function () {

  describe("constructor()", function () {

    it("Should create a address object if no args are passed in.", function (done) {

      const address = new ZillowAddress()
      expect(address).to.be.an.instanceOf(ZillowAddress)
      done()

    })

    it("Should create a address object if all args are passed.", function (done) {

      const address = new ZillowAddress({
        street: "123 Main Street",
        zipcode: 92620,
        city: "Irvine",
        state: "CA",
        latitude: -123456,
        longitude: 123456
      })
      expect(address).to.be.an.instanceOf(ZillowAddress)
      done()

    })

  })

  describe("setStreet() and getStreet()", function () {

    it("Should still work if no args are passed.", function (done) {

      const address = new ZillowAddress()
      address.setStreet()
      assert.equal(address.getStreet(), undefined)
      done()

    })


    it("Should set street properly if street is passed.", function (done) {

      const address = new ZillowAddress()
      address.setStreet("123 Main Street")
      assert.equal(address.getStreet(), "123 Main Street")
      done()

    })

  })

  describe("setZipcode() and getZipcode()", function () {

    it("Should be undefined if no args are passed.", function (done) {

      const address = new ZillowAddress()
      address.setZipcode()
      assert.equal(address.getZipcode(), undefined)
      done()

    })

    it("Should set zipcode properly if zipcode is passed.", function (done) {

      const address = new ZillowAddress()
      address.setZipcode(50000)
      assert.equal(address.getZipcode(), 50000)
      done()

    })

  })

  describe("setCity() and getCity()", function () {

    it("Should still work if no args are passed.", function (done) {

      const address = new ZillowAddress()
      address.setCity()
      assert.equal(address.getCity(), undefined)
      done()

    })


    it("Should set city properly if city is passed.", function (done) {

      const address = new ZillowAddress()
      address.setCity("Irvine")
      assert.equal(address.getCity(), "Irvine")
      done()

    })

  })

  describe("setState() and getState()", function () {

    it("Should still work if no args are passed.", function (done) {

      const address = new ZillowAddress()
      address.setState()
      assert.equal(address.getState(), undefined)
      done()

    })


    it("Should set state properly if state is passed.", function (done) {

      const address = new ZillowAddress()
      address.setState("CA")
      assert.equal(address.getState(), "CA")
      done()

    })

  })

  describe("setLatitude() and getLatitude()", function () {

    it("Should be undefined if no args are passed.", function (done) {

      const address = new ZillowAddress()
      address.setLatitude()
      assert.equal(address.getLatitude(), undefined)
      done()

    })

    it("Should set latitude properly if latitude is passed.", function (done) {

      const address = new ZillowAddress()
      address.setLatitude(50000)
      assert.equal(address.getLatitude(), 50000)
      done()

    })

  })

  describe("setLongitude() and getLongitude()", function () {

    it("Should be undefined if no args are passed.", function (done) {

      const address = new ZillowAddress()
      address.setLongitude()
      assert.equal(address.getLongitude(), undefined)
      done()

    })

    it("Should set longitude properly if longitude is passed.", function (done) {

      const address = new ZillowAddress()
      address.setLongitude(-123)
      assert.equal(address.getLongitude(), -123)
      done()

    })

  })

  describe("toObject()", function () {

    it("Should return object representation of address.", function (done) {

      const addressComp = new ZillowAddress({
        street: "123 Main Street",
        zipcode: 92620,
        city: "Irvine",
        state: "CA",
        latitude: -123456,
        longitude: 123456
      })
      expect(addressComp.toObject()).to.be.an("object")
      assert.equal(addressComp.toObject().street, "123 Main Street")
      assert.equal(addressComp.toObject().zipcode, 92620)
      assert.equal(addressComp.toObject().city, "Irvine")
      assert.equal(addressComp.toObject().state, "CA")
      assert.equal(addressComp.toObject().latitude, -123456)
      assert.equal(addressComp.toObject().longitude, 123456)
      done()

    })

  })

})