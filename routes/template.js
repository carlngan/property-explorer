"use strict"
const express = require('express')
const router = express.Router()

router.get('/search', function (req, res) {

  res.render('search')

})

router.get('/overview', function (req, res) {

  res.render('overview')

})

module.exports = router
