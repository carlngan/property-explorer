"use strict"
const express = require('express')
const router = express.Router()
const Promise = require('bluebird')
const nodemailer = require('nodemailer')

const config = require('./../config')
const ZillowJsonSDK = require('./../src/ZillowJsonSDK')
const zjs = new ZillowJsonSDK(config.zillowApi)

const googleMapsClient = require('@google/maps').createClient({
  key: config.googleMapsGeocodingApi.key
})

router.post('/search', (req, res) => {

  zjs.getSearchResults(req.body)
    .then((data) => {

      res.send(data)

    })
    .catch((err) => {
      console.log("CATCH", err)
      res.status(500).send(err)
    })
    .error((err) => {
      console.log("ERROR", err)
      res.status(500).send(err)
    })

})

router.post('/address', (req, res) => {

  googleMapsClient.geocode({
    address: req.body.text
  }, function (err, response) {
    if (!err) {
      return res.send(response.json.results)
    }
    console.log("ERROR", err)
  })

})

const transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: "propertyexplorer2017@gmail.com",
    pass: "notaproperty"
  }
})

const mailOptions = {
  from: 'Property Explorer<"propertyexplorer2017@gmail.com">', // sender address
  to: 'carl@salonfrontdesk.com', // list of receivers
  subject: '[Zillow Property Explorer] Contact Form'
}

const sendMail = function (callback) {
  transporter.sendMail(mailOptions, function (error, info) {
    callback(error, info)
  })
}

router.post('/contact', (req, res) => {

  mailOptions.to = mailOptions.to + "," + req.body.email
  mailOptions.html = ""
  const keys = Object.keys(req.body)
  for (let i = 0; i < keys.length; i++) {
    mailOptions.html = mailOptions.html + "<strong>" + keys[i] + ": </strong>" + JSON.stringify(req.body[keys[i]]) + "<br>"
  }
  sendMail(function (err, data) {

    if (err) {
      console.log("ERROR", err)
      res.status(500).send({msg: err})
    }
    else {
      res.status(200).end()
    }

  })

})

module.exports = router