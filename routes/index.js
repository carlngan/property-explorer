"use strict"

const express = require('express')
const router = express.Router()

/* GET layout page. */
router.get('/*', (req, res) => {

  res.render("layout")

})

module.exports = router