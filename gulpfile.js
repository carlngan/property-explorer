const gulp = require('gulp')
const sass = require('gulp-sass')
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const cssmin = require('gulp-cssmin')

// Compile Our Sass
gulp.task('sass', function () {

  gulp.src('scss/style.scss')
    .pipe(sass())
    .pipe(gulp.dest('public/css'))
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/css'))

})

// Watch Files For Changes
gulp.task('watch', function () {

  return gulp.watch('scss/*.scss', ['sass'])

})

// Default Task
gulp.task('default', ['sass'])