# Zillow Property Explorer

[![Demo](http://www.sachinghare.com/wp-content/uploads/2013/11/demo-button.jpg)](https://property-explorer.carlngan.com)

Zillow Property Explorer is a tool that allows users to quickly get an overview of a piece of property. It comes with just enough information to get grasp of the property with everything else available in detail through linking to external pages.

The project was built with time and other constraints, so it has a tremendous amount of room for improvements.
### Links

* [Demo] - Demo of this app in production!
* [User Walkthrough] - Walk-through of the site from the users perspective, and a user manual.
* [Documentation] - Documentation of significant objects and classes.
* [Code Coverage] - How much our tests cover the code base.

### Technologies

Zillow Property Explorer uses a number of open source projects to work properly:

* [Node.js] - JavaScript for the backend.
* [ExpressJs] - Fast, unopinionated, minimalist web framework for Node.js
* [AngularJs] - Javascript Framework
* [AngularMaterial] - Both a UI Component framework and a reference implementation of Google's Material Design Specification
* [Pug] - Templating engine for html.
* [Scss] - Syntax known as SCSS which is fully compatible with the syntax of CSS, while still supporting the full power of Sass
* [JsDocs] - Documentation generation for JavaScript
* [Istanbul] - Code Coverage tool
* [MochaJs] - Feature-rich JavaScript test framework running on Node.js
* [Zillow API] - Finds a property for a specified address
* [Google Maps Geocoding API] - Geocoding converts addresses into geographic coordinates to be placed on a map
* [AWS EC2] - Web service that provides secure, resizable compute capacity in the cloud
* [AWS CodingDeploy] - Automates code deployments to any instance
* [Bitbucket] - Git repository
* [PM2] - Advanced, production process manager for Node.js

### Installation

Zillow Property Explorer requires [Node.js](https://nodejs.org/) v6+ to run.

For development environments:
Configurations and port settings can be found at: /config/development.json
```sh
$ cd property-explorer
$ sudo npm install
$ npm run bower
$ npm run gulp
$ npm start
```
Then navigate to http://localhost:{port}

### Credits

A number of external resources and ideas are borrowed, used, or referenced in the making of Zillow Property Explorer.

| Resource | Type | From |
| ------ | ------ | ------ |
| Markdown Generation | Idea | http://dillinger.io/ |
| Landing Page Background Video | Video | https://www.zillow.com/ |
| Font Gotham | Font, Idea | https://www.zillow.com/ |
| zsg-core.css | CSS | https://www.zillow.com/ |
| zsg-opt.css | CSS | https://www.zillow.com/ |
| Property Overview UX/UI Layout | Idea | https://www.zillow.com/ |
| Property Styling | Idea | https://www.zillow.com/ |
| Zillow Logo | Image | https://www.zillow.com/ |
| Google Maps Image | Image | https://www.maps.google.com |
| Graphs and Data Image | Image | https://www.purrmetrix.com |

### Known bugs / Missing Features

- Unable to use keyboard to navigate through auto complete list on search
- Unable to use tab / enter key on search.

### Development Process

##### User Stories

Constraints:
* The output should be a cleanly-displayed representation of whatever came back in the API.
* UI should look reasonable.
* If the API responds with an error for some reason, the user should be informed and not left hanging.
* AWS, bitbucket.
* A few hours.

A user is someone who wants to see the details of a particular piece of property by providing an address. Specifically, fellows who are facilitating this interview.

As a user:

* I need to:
    - Be able to enter in an address or parts of an address to search
    - Be able to see and compare rental information as well.
    - When a home is searched and found:
    - See the Zillow Property ID.
    - Have a link to:
        * The home details page.
        * The charts and data page.
        * A map of where this home is on.
    - See similar homes.
    - See the full address to this home with latitude and longitude.
    - See the zestimate:
        * Value in $
        * When it is last updated.
        * Changes in the last 30 days.
        * Valuation range (low to high)
        * Percentile value
    - See the zestimate (Rental -- if selected)):
        * Value in $
        * When it is last updated.
        * Changes in the last 30 days.
        * Valuation range (low to high)
    - See error messages with possible actions if there is one.
* I want to:
    - Auto fill/recommend full addresses while I type
    - See some tips/help on inputting the address
    - Be able to control whether I see zestimate rental information
    - Be able to quickly look up/search another property while looking at a property without click on �back�
* It would be nice to:
    - Contact the people behind this app.
    - Preview on links I hover over to get a better understanding

##### System / Process Maps

![System Map](https://property-explorer.carlngan.com/img/img_system.jpg)

![Flow Diagram](https://property-explorer.carlngan.com/img/img_flow.jpg)

##### Wireframes

![Screen 1](https://property-explorer.carlngan.com/img/img_frame1.jpg)

![Screen 2](https://property-explorer.carlngan.com/img/img_frame2.jpg)

##### Sequence / UML Diagram

![Sequence](https://property-explorer.carlngan.com/img/img_sequence.jpg)

   [Demo]: <https://property-explorer.carlngan.com>
   [Documentation]: <https://property-explorer.carlngan.com/doc>
   [User Walkthrough]: <>
   [Code Coverage]: <https://property-explorer.carlngan.com/coverage/lcov-report>
   [Node.js]: <http://nodejs.org>
   [ExpressJs]: <https://expressjs.com/>
   [AngularJs]: <https://angularjs.org/>
   [AngularMaterial]: <https://material.angularjs.org/latest/>
   [Pug]: <https://pugjs.org/api/getting-started.html>
   [Scss]: <http://sass-lang.com/documentation/file.SCSS_FOR_SASS_USERS.html>
   [JsDocs]: <http://usejsdoc.org/>
   [Istanbul]: <https://github.com/gotwarlost/istanbul>
   [MochaJs]: <https://mochajs.org/>
   [Zillow API]: <https://www.zillow.com/howto/api/GetSearchResults.htm>
   [Google Maps Geocoding API]: <https://developers.google.com/maps/documentation/geocoding/intro>
   [AWS EC2]: <https://aws.amazon.com/ec2/>
   [AWS CodingDeploy]: <https://aws.amazon.com/codedeploy/>
   [Bitbucket]: <https://bitbucket.org/>
   [PM2]: <http://pm2.keymetrics.io/>
