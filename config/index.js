const path = require('path')
const _ = require('lodash')

const env = process.env.NODE_ENV || 'development'
let config
config = require(`./${env}.json`)

const defaults = {
  root: path.join(__dirname, '/..')
};

_.assign(config, defaults)

module.exports = config