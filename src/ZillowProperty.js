'use strict'

const ZillowAddress = require('./ZillowAddress')
const ZillowValuation = require('./ZillowValuation')
const ZillowCommunity = require('./ZillowCommunity')

/**
 * The zpid of this property.
 * @type {Number}
 * @private
 */
const zpid = new WeakMap()
/**
 * The homeDetails link for this property.
 * @type {String}
 * @private
 */
const homeDetails = new WeakMap()
/**
 * The graphsAndData link for this property.
 * @type {String}
 * @private
 */
const graphsAndData = new WeakMap()
/**
 * The mapThisHome link for this property.
 * @type {String}
 * @private
 */
const mapThisHome = new WeakMap()
/**
 * The comparables link for this property.
 * @type {String}
 * @private
 */
const comparables = new WeakMap()
/**
 * The address of this property.
 * @type {ZillowAddress}
 * @private
 */
const address = new WeakMap()
/**
 * The zestimate valuation of this property.
 * @type {ZillowValuation}
 * @private
 */
const zestimate = new WeakMap()
/**
 * The zestimate rental valuation of this property.
 * @type {ZillowValuation}
 * @private
 */
const rentZestimate = new WeakMap()
/**
 * The community of this property.
 * @type {ZillowCommunity}
 * @private
 */
const localRealEstate = new WeakMap()

/**
 * Class representing a Zillow Property.
 */
class ZillowProperty {

  /**
   * @constructor
   *
   * @param {Object} args - Argument passed in as object.
   * @param {Number} args.zpid - The zpid of this property.
   * @param {String} args.homedetails - The homeDetails link for this property.
   * @param {String} args.graphsanddata - The graphsanddata link for this property.
   * @param {String} args.mapthishome - The mapthishome link for this property.
   * @param {String} args.comparables - The comparables link for this property.
   * @param {Object} args.address - The address object of this property.
   * @param {Object} args.zestimate - The zestimate valuation object of this property.
   * @param {Object} args.rentzestimate - The zestimate rental valuation object of this property.
   * @param {Object} args.localRealEstate - The community object of this property.
   *
   * @returns {Void}
   */
  constructor(args = {}) {

    if (args.zpid) {
      this.setZpid(args.zpid)
    }
    if (args.homedetails) {
      this.setHomeDetails(args.homedetails)
    }
    if (args.graphsanddata) {
      this.setGraphsAndData(args.graphsanddata)
    }
    if (args.mapthishome) {
      this.setMapThisHome(args.mapthishome)
    }
    if (args.comparables) {
      this.setComparables(args.comparables)
    }
    if (args.address) {
      this.setAddress(new ZillowAddress(args.address))
    }
    if (args.zestimate) {
      this.setZestimate(new ZillowValuation(args.zestimate))
    }
    if (args.rentzestimate) {
      this.setRentZestimate(new ZillowValuation(args.rentzestimate))
    }
    if (args.localRealEstate && args.localRealEstate.region) {
      this.setLocalRealEstate(new ZillowCommunity(args.localRealEstate.region))
    }

  }

  /**
   * Set the zpid for this property.
   *
   * @param {Number} z - The zpid of this property.
   *
   * @returns {Void}
   */
  setZpid(z) {

    zpid.set(this, z)

  }

  /**
   * Get the zpid for this property.
   *
   * @returns {Number}
   */
  getZpid() {

    return zpid.get(this)

  }

  /**
   * Set the homeDetails link for this property.
   *
   * @param {String} h - The homeDetails link for this property.
   *
   * @returns {Void}
   */
  setHomeDetails(h) {

    homeDetails.set(this, h)

  }

  /**
   * Get the homeDetails link for this property.
   *
   * @returns {String}
   */
  getHomeDetails() {

    return homeDetails.get(this)

  }

  /**
   * Set the graphsAndData link for this property.
   *
   * @param {String} g - The graphsAndData link for this property.
   *
   * @returns {Void}
   */
  setGraphsAndData(g) {

    graphsAndData.set(this, g)

  }

  /**
   * Get the graphsAndData link for this property.
   *
   * @returns {String}
   */
  getGraphsAndData() {

    return graphsAndData.get(this)

  }

  /**
   * Set the mapThisHome link for this property.
   *
   * @param {String} m - The mapThisHome link for this property.
   *
   * @returns {Void}
   */
  setMapThisHome(m) {

    mapThisHome.set(this, m)

  }

  /**
   * Get the mapThisHome link for this property.
   *
   * @returns {String}
   */
  getMapThisHome() {

    return mapThisHome.get(this)

  }

  /**
   * Set the comparables link for this property.
   *
   * @param {String} c - The comparables link for this property.
   *
   * @returns {Void}
   */
  setComparables(c) {

    comparables.set(this, c)

  }

  /**
   * Get the comparables link for this property.
   *
   * @returns {String}
   */
  getComparables() {

    return comparables.get(this)

  }

  /**
   * Set the address for this property.
   *
   * @param {ZillowAddress} a - The address of this property.
   *
   * @returns {Void}
   */
  setAddress(a) {

    address.set(this, a)

  }

  /**
   * Get the address for this property.
   *
   * @returns {ZillowAddress}
   */
  getAddress() {

    return address.get(this)

  }

  /**
   * Set the zestimate valuation for this property.
   *
   * @param {ZillowValuation} z - The zestimate valuation for this property.
   *
   * @returns {Void}
   */
  setZestimate(z) {

    zestimate.set(this, z)

  }

  /**
   * Get the zestimate valuation for this property.
   *
   * @returns {ZillowValuation}
   */
  getZestimate() {

    return zestimate.get(this)

  }

  /**
   * Set the zestimate rental valuation for this property.
   *
   * @param {ZillowValuation} r - The zestimate rental valuation for this property.
   *
   * @returns {Void}
   */
  setRentZestimate(r) {

    rentZestimate.set(this, r)

  }

  /**
   * Get the zestimate rental valuation for this property.
   *
   * @returns {ZillowValuation}
   */
  getRentZestimate() {

    return rentZestimate.get(this)

  }

  /**
   * Set the local real estate community for this property.
   *
   * @param {ZillowCommunity} l - The local real estate community for this property.
   *
   * @returns {Void}
   */
  setLocalRealEstate(l) {

    localRealEstate.set(this, l)

  }

  /**
   * Get the local real estate community for this property.
   *
   * @returns {ZillowCommunity}
   */
  getLocalRealEstate() {

    return localRealEstate.get(this)

  }

  /**
   * Get the object representation for this property.
   *
   * @returns {Object}
   */
  toObject(format = false) {

    return {
      zpid: this.getZpid(),
      links: {
        homeDetails: this.getHomeDetails(),
        graphsAndData: this.getGraphsAndData(),
        mapThisHome: this.getMapThisHome(),
        comparables: this.getComparables()
      },
      address: this.getAddress().toObject(),
      zestimate: this.getZestimate().toObject(format),
      rentzestimate: this.getRentZestimate() ? this.getRentZestimate().toObject(format) : null,
      localRealEstate: this.getLocalRealEstate().toObject()
    }

  }

}

module.exports = ZillowProperty