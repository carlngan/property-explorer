'use strict'

/**
 * The street for this address.
 * @type {String}
 * @private
 */
const street = new WeakMap()
/**
 * The zipcode for this address.
 * @type {Number}
 * @private
 */
const zipcode = new WeakMap()
/**
 * The city for this address.
 * @type {String}
 * @private
 */
const city = new WeakMap()
/**
 * The state for this address.
 * @type {String}
 * @private
 */
const state = new WeakMap()
/**
 * The latitude for this address.
 * @type {Number}
 * @private
 */
const latitude = new WeakMap()
/**
 * The longitude for this address.
 * @type {Number}
 * @private
 */
const longitude = new WeakMap()

/**
 * Class representing a Zillow Address.
 */
class ZillowAddress {

  /**
   * @constructor
   *
   * @param {Object} args - Argument passed in as object.
   * @param {String} args.street - The street for this address.
   * @param {Number} args.zipcode - The zipcode for this address.
   * @param {String} args.city - The city for this address.
   * @param {String} args.state - The state for this address.
   * @param {Number} args.latitude - The latitude for this address.
   * @param {Number} args.longitude - The longitude for this address.
   *
   * @returns {Void}
   */
  constructor(args = {}) {

    if (args.street) {
      this.setStreet(args.street)
    }
    if (args.zipcode) {
      this.setZipcode(args.zipcode)
    }
    if (args.city) {
      this.setCity(args.city)
    }
    if (args.state) {
      this.setState(args.state)
    }
    if (args.latitude) {
      this.setLatitude(args.latitude)
    }
    if (args.longitude) {
      this.setLongitude(args.longitude)
    }

  }

  /**
   * Set the street for this address.
   *
   * @param {String} s - The street for this address.
   *
   * @returns {Void}
   */
  setStreet(s) {

    street.set(this, s)

  }

  /**
   * Get the street for this address.
   *
   * @returns {String}
   */
  getStreet() {

    return street.get(this)

  }

  /**
   * Set the zip code for this address.
   *
   * @param {Number} z - Teh zip code for this address.
   *
   * @returns {Void}
   */
  setZipcode(z) {

    zipcode.set(this, z)

  }

  /**
   * Get the zip code for this address.
   *
   * @returns {Number}
   */
  getZipcode() {

    return zipcode.get(this)

  }

  /**
   * Set the city for this address.
   *
   * @param {String} c - The city for this address.
   *
   * @returns {Void}
   */
  setCity(c) {

    city.set(this, c)

  }

  /**
   * Get the city for this address.
   *
   * @returns {String}
   */
  getCity() {

    return city.get(this)

  }

  /**
   * Set the state for this address.
   *
   * @param {String} s - The state for this address.
   *
   * @returns {Void}
   */
  setState(s) {

    state.set(this, s)

  }

  /**
   * Get teh state for this address.
   *
   * @returns {String}
   */
  getState() {

    return state.get(this)

  }

  /**
   * Set the latitude for this address.
   *
   * @param {Number} l - Status for this address.
   *
   * @returns {Void}
   */
  setLatitude(l) {

    latitude.set(this, l)

  }

  /**
   * Get the latitude for this address.
   *
   * @returns {Number}
   */
  getLatitude() {

    return latitude.get(this)

  }

  /**
   * Set the longitude for this address.
   *
   * @param {Number} l - The longitude for this address.
   *
   * @returns {Void}
   */
  setLongitude(l) {

    longitude.set(this, l)

  }

  /**
   * Get the longitude for this address.
   *
   * @returns {Number}
   */
  getLongitude() {

    return longitude.get(this)

  }


  /**
   * Get the object representation for this address.
   *
   * @returns {Object}
   */
  toObject() {

    return {
      street: this.getStreet(),
      zipcode: this.getZipcode(),
      city: this.getCity(),
      state: this.getState(),
      latitude: this.getLatitude(),
      longitude: this.getLongitude(),
    }

  }

}

module.exports = ZillowAddress