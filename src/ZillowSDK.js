'use strict'

const Promise = require('bluebird')
const _ = require('lodash')
const request = require('request-promise')

const ZillowMessage = require('./ZillowMessage')
const ZillowProperty = require('./ZillowProperty')

/**
 * Class representing a Zillow SDK as a bridge to the API.
 */
class ZillowSDK {

  /**
   * @constructor
   *
   * @param {Object} zillowApi - Argument passed in as object.
   * @param {String} zillowApi.url - The API URL for this config.
   * @param {String} zillowApi.key - The API Key for this config.
   *
   * @returns {Void}
   */
  constructor(zillowApi = {}) {

    if (!zillowApi.url || !zillowApi.key) {

      throw new Error('Zillow API Config Required')

    }

    this.apiUrl = zillowApi.url
    this.apiKey = zillowApi.key

  }

  /**
   * Get the search results for an address.
   *
   * @param {Object} params - Argument passed in as object.
   * @param {String} params.address - The address for this search.
   * @param {String} params.city - The city for this search.
   * @param {String} params.state - The state for this search.
   * @param {String} params.zip - The zip for this search.
   * @param {Boolean} params.rentzestimate - Whether we want to know how zestimate rental information as well.
   *
   * @returns {Promise}
   */
  getSearchResults(params) {

    return new Promise((resolve, reject) => {

      this.validateParams(params)
        .then((processedParams) => {

          const options = {
            uri: this.apiUrl,
            qs: {
              "zws-id": this.apiKey,
              "address": processedParams.address,
              "citystatezip": processedParams.citystatezip,
              "rentzestimate": processedParams.rentzestimate
            }
          }

          return resolve(request(options))

        })
        .catch((e) => {
          reject(e)
        })
        .error((e) => {
          reject(e)
        })

    })

  }

  /**
   * Validate and process the parameter to choose either city + state or zip.
   *
   * @param {Object} params - Argument passed in as object.
   * @param {String} params.address - The address for this search.
   * @param {String} params.city - The city for this search.
   * @param {String} params.state - The state for this search.
   * @param {String} params.zip - The zip for this search.
   * @param {Boolean} params.rentzestimate - Whether we want to know how zestimate rental information as well.
   *
   * @returns {Promise<Object, Error>}
   */
  validateParams(params) {

    return new Promise((resolve, reject) => {

      const res = {}

      if (!_.isString(params.address)) {

        return reject(new TypeError("String expected for address."))

      }

      res.address = params.address

      if (params.city && params.state) {

        if (!_.isString(params.city) || !_.isString(params.state)) {

          return reject(new TypeError("String expected for city and state."))

        }

        res.citystatezip = params.city + " " + params.state

      }
      else {

        if (!_.isNumber(params.zip)) {

          return reject(new TypeError("Number expected for zip."))

        }

        res.citystatezip = params.zip

      }

      if (params.rentzestimate && !_.isBoolean(Boolean(params.rentzestimate))) {

        return reject(new TypeError("Boolean expected for rentzestimate."))

      }
      else if (params.rentzestimate) {

        res.rentzestimate = params.rentzestimate

      }

      return resolve(res)

    })

  }

  /**
   * Build a response object for the information we care about.
   *
   * @param {Object} rawData - Object from Zillow API call.
   *
   * @returns {Object}
   */
  buildResponse(rawData) {

    const data = rawData["SearchResults:searchresults"]
    const zm = new ZillowMessage(data.message)
    let zp

    if (zm.getCode() === 0) {

      const result = data.response.results.result;
      zp = new ZillowProperty({
        zpid: result.zpid,
        homedetails: result.links ? result.links.homedetails : null,
        graphsanddata: result.links ? result.links.graphsanddata : null,
        mapthishome: result.links ? result.links.mapthishome : null,
        comparables: result.links ? result.links.comparables : null,
        address: result.address ? result.address : null,
        zestimate: result.zestimate ? result.zestimate : null,
        rentzestimate: result.rentzestimate ? result.rentzestimate : null,
        localRealEstate: result.localRealEstate ? result.localRealEstate : null
      })

    }

    return {

      message: zm.toObject(),
      result: zm.getCode() === 0 ? zp.toObject(true) : null

    }

  }

}

module.exports = ZillowSDK