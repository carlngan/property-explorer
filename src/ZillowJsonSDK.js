'use strict'

const Promise = require('bluebird')

const XmlParser = require('./XmlParser')
const ZillowSDK = require('./ZillowSDK')

/**
 * Class representing a Zillow SDK in JSON.
 */
class ZillowJsonSDK extends ZillowSDK {

  /**
   * @constructor
   *
   * @param {Object} zillowApi - Argument passed in as object.
   * @param {String} zillowApi.url - The API URL for this config.
   * @param {String} zillowApi.key - The API Key for this config.
   *
   * @returns {Void}
   */
  constructor(zillowApi = {}) {

    super(zillowApi)

  }

  /**
   * Get the search results for an address.
   *
   * @param {Object} params - Argument passed in as object.
   * @param {String} params.address - The address for this search.
   * @param {String} params.city - The city for this search.
   * @param {String} params.state - The state for this search.
   * @param {String} params.zip - The zip for this search.
   * @param {Boolean} params.rentzestimate - Whether we want to know how zestimate rental information as well.
   *
   * @returns {Promise}
   */
  getSearchResults(params) {

    return new Promise((resolve, reject) => {

      super.getSearchResults(params)
        .then((results) => {

          const obj = XmlParser.toJson(results)
          return resolve(this.buildResponse(obj))

        })
        .catch((e) => {
          reject(e)
        })
        .error((e) => {
          reject(e)
        })
    })

  }

}

module.exports = ZillowJsonSDK