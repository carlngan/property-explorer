'use strict'

/**
 * The text of this message.
 * @type {String}
 * @private
 */
const text = new WeakMap()
/**
 * The code of this message.
 * @type {Number}
 * @private
 */
const code = new WeakMap()
/**
 * The resolution of this message.
 * @type {String}
 * @private
 */
const resolution = new WeakMap()

/**
 * Class representing an message.
 */
class ZillowMessage {

  /**
   * @constructor
   *
   * @param {Object} args - Argument passed in as object.
   * @param {String} args.text - The text of this message.
   * @param {String} args.code - The code of this message.
   *
   * @returns {Void}
   */
  constructor(args = {}) {

    if (args.text) {
      this.setText(args.text)
    }
    if (args.code) {
      this.setCode(args.code)
    }

  }

  /**
   * Set the text for this message.
   *
   * @param {String} t - The text for this message.
   *
   * @returns {Void}
   */
  setText(t) {

    text.set(this, t)

  }

  /**
   * Get the text for this message.
   *
   * @returns {String}
   */
  getText() {

    return text.get(this)

  }

  /**
   * Set the code for this message.
   *
   * @param {Number} c - The code for this message as well as the resolution.
   *
   * @returns {Void}
   */
  setCode(c) {

    c = Number(c)
    code.set(this, c)
    let resolution = ""

    switch(c) {

      case 0:
        break
      case 1:
        resolution = "Check to see if your address is properly formed: delimiter, character cases, etc."
        break
      case 2:
        resolution = "Something is wrong on our end. A report has been submitted."
        //report
        break
      case 3:
        resolution = "This service is currently not available. Please come back later and try again."
        break
      case 4:
        resolution = "This service is currently not available. Please come back later and try again."
        break
      case 500:
        resolution = "Check to see if the address is correct. When inputting a city name, include the " +
          "state too. A city name alone will not result in a valid address."
        break
      case 501:
        resolution = "Check to see if the address is correct. When inputting a city name, include the " +
          "state too. A city name alone will not result in a valid address."
        break
      case 502:
        resolution = "Sorry, the address you provided is not found in the Zillow's property database."
        break
      case 503:
        resolution = "Please check to see if the city/state you entered is valid. If you provided a ZIP code, check to see if it is valid.."
        break
      case 504:
        resolution = "The specified area is not covered by the Zillow property database. To see our " +
          "property coverage tables, go to: https://www.zillow.com/zestimate/#acc"
        break
      case 505:
        resolution = "Your request timed out. The server could be busy or unavailable. Try again later."
        break
      case 506:
        resolution = "Address is too long. If address is valid, try using abbreviations."
        break
      case 507:
        resolution = "No exact match found. Verify that the given address is correct."
        break
      case 508:
        resolution = "Verify that the given address is correct. Please contact us if the issue persists."
        break
      default:
        resolution = "No idea what happened."
        break

    }

    this.setResolution(resolution)

  }

  /**
   * Get the code for this message.
   *
   * @returns {Number}
   */
  getCode() {

    return code.get(this)

  }

  /**
   * Set the resolution for this message.
   *
   * @param {String} r - The resolution for this message.
   *
   * @returns {Void}
   */
  setResolution(r) {

    resolution.set(this, r)

  }

  /**
   * Get the resolution for this message.
   *
   * @returns {String}
   */
  getResolution() {

    return resolution.get(this)

  }

  /**
   * Get the object representation for this message.
   *
   * @returns {Object}
   */
  toObject() {

    return {
      text: this.getText(),
      code: this.getCode(),
      resolution: this.getResolution()
    }

  }

}

module.exports = ZillowMessage