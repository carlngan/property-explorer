'use strict'

/**
 * The currency for this money.
 * @type {String}
 * @private
 */
const currency = new WeakMap()
/**
 * The amount of money.
 * @type {Number}
 * @private
 */
const amount = new WeakMap()


/**
 * Class representing an money.
 */
class ZillowMoney {

  /**
   * @constructor
   *
   * @param {Object} args - Argument passed in as object.
   * @param {String} args.currency - The currency for this money.
   * @param {String} args.amount - The amount of money.
   *
   * @returns {Void}
   */
  constructor(args = {}) {

    if (args.currency) {
      this.setCurrency(args.currency)
    }
    if (args.amount) {
      this.setAmount(args.amount)
    }

  }

  /**
   * Set the currency for this money.
   *
   * @param {String} c - The currency for this money.
   *
   * @returns {Void}
   */
  setCurrency(c) {

    currency.set(this, c)

  }

  /**
   * Get the currency for this money.
   *
   * @returns {String}
   */
  getCurrency() {

    return currency.get(this)

  }

  /**
   * Set the amount for this money.
   *
   * @param {Number} a - The amount of money.
   *
   * @returns {Void}
   */
  setAmount(a = 0) {

    amount.set(this, Number(a))

  }

  /**
   * Get the amount for this money.
   *
   * @returns {Number}
   */
  getAmount() {

    return amount.get(this)

  }

  /**
   * Get the amount of money in string format.
   *
   * @returns {String}
   */
  getMoneyFormat() {

    let res = ""

    if (this.getAmount() < 0) {

      res = res.concat("-")

    }
    if (this.getCurrency() === "USD") {

      res = res.concat("$")

    }

    res = res.concat(Math.abs(this.getAmount()).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))

    return res

  }

  /**
   * Get the + or - change in currency amount.
   *
   * @returns {String}
   */
  getChangeFormat() {

    if (this.getAmount() < 0) {

      return this.getMoneyFormat()

    }

    let res = "+"

    res = res.concat(this.getMoneyFormat())

    return res

  }

  /**
   * Get the object representation for this money.
   *
   * @returns {Object}
   */
  toObject(format = false) {

    return {
      currency: this.getCurrency(),
      amount: format ? this.getMoneyFormat() : this.getAmount()
    }

  }

}

module.exports = ZillowMoney