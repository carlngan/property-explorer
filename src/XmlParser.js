'use strict'

const parser = require('xml2json')

/**
 * Class representing an XML parser.
 */
class XmlParser {

  /**
   * Converts XML to JSON string.
   *
   * @param {String} xml - XML information passed in as a string.
   *
   * @returns {String}
   */
  static toJsonString(xml) {

    return parser.toJson(xml)

  }

  /**
   * Converts XML to JSON object.
   *
   * @param {String} xml - XML information passed in as a string.
   *
   * @returns {Object}
   */
  static toJson(xml) {

    return JSON.parse(parser.toJson(xml))

  }

}

module.exports = XmlParser