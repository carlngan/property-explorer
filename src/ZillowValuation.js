'use strict'

const ZillowMoney = require('./ZillowMoney')

/**
 * The amount of this valuation.
 * @type {ZillowMoney}
 * @private
 */
const amount = new WeakMap()
/**
 * The time this valuation was last updated.
 * @type {String}
 * @private
 */
const lastUpdated = new WeakMap()
/**
 * The value change duration of this valuation.
 * @type {Number}
 * @private
 */
const valueChangeDuration = new WeakMap()
/**
 * The value change amount of this valuation.
 * @type {ZillowMoney}
 * @private
 */
const valueChangeAmount = new WeakMap()
/**
 * The valuation low range of this valuation.
 * @type {ZillowMoney}
 * @private
 */
const valuationRangeLow = new WeakMap()
/**
 * The valuation high range of this valuation.
 * @type {ZillowMoney}
 * @private
 */
const valuationRangeHigh = new WeakMap()
/**
 * The percentile of this valuation.
 * @type {Number}
 * @private
 */
const percentile = new WeakMap()


/**
 * Class representing a Zillow Valuation.
 */
class ZillowValuation {

  /**
   * @constructor
   *
   * @param {Object} args - Argument passed in as object.
   * @param {Object} args.amount - The amount object of this valuation.
   * @param {String} args.amount.currency - The currency of this valuation amount.
   * @param {Number} args.amount.$t - The amount of this valuation.
   * @param {String} args.last-updated - The time this valuation was last updated.
   * @param {Object} args.valueChange - The value change object of this valuation.
   * @param {String} args.valueChange.currency - The value change currency of this valuation.
   * @param {Number} args.valueChange.$t - The value change amount of this valuation.
   * @param {Object} args.valuationRange - The valuation range object of this valuation.
   * @param {Object} args.valuationRange.low - The low valuation range object of this valuation.
   * @param {String} args.valuationRange.low.currency - The low valuation range currency of this valuation.
   * @param {Number} args.valuationRange.low.$t - The low valuation range amount of this valuation.
   * @param {Object} args.valuationRange.high - The high valuation range object of this valuation.
   * @param {String} args.valuationRange.high.currency - The high valuation range currency of this valuation.
   * @param {Number} args.valuationRange.high.$t - The high valuation range amount of this valuation.
   * @param {Number} args.percentile - The percentile of this valuation.
   *
   * @returns {Void}
   */
  constructor(args = {}) {

    if (args.amount) {
      this.setAmount(new ZillowMoney({
        currency: args.amount.currency,
        amount: args.amount.$t
      }))
    }
    if (args["last-updated"]) {
      this.setLastUpdated(args["last-updated"])
    }
    if (args.valueChange) {
      this.setValueChangeDuration(args.valueChange.duration)
      this.setValueChangeAmount(new ZillowMoney({
        currency: args.valueChange.currency,
        amount: args.valueChange.$t
      }))
    }
    if (args.valuationRange) {
      if (args.valuationRange.low) {
        this.setValuationRangeLow(new ZillowMoney({
          currency: args.valuationRange.low.currency,
          amount: args.valuationRange.low.$t
        }))
      }
      if (args.valuationRange.high) {
        this.setValuationRangeHigh(new ZillowMoney({
          currency: args.valuationRange.high.currency,
          amount: args.valuationRange.high.$t
        }))
      }
    }
    if (args.percentile) {
      this.setPercentile(args.percentile)
    }

  }

  /**
   * Set the amount for this valuation.
   *
   * @param {ZillowMoney} a - The amount of this valuation.
   *
   * @returns {Void}
   */
  setAmount(a) {

    amount.set(this, a)

  }

  /**
   * Get the amount for this valuation.
   *
   * @returns {ZillowMoney}
   */
  getAmount() {

    return amount.get(this)

  }

  /**
   * Set when this valuation was last updated.
   *
   * @param {String} l - The time this valuation was last updated.
   *
   * @returns {Void}
   */
  setLastUpdated(l) {

    lastUpdated.set(this, l)

  }

  /**
   * Get when this valuation was last updated.
   *
   * @returns {String}
   */
  getLastUpdated() {

    return lastUpdated.get(this)

  }

  /**
   * Set the value change duration for this valuation.
   *
   * @param {Number} v - The valuation change duration of this valuation.
   *
   * @returns {Void}
   */
  setValueChangeDuration(v) {

    valueChangeDuration.set(this, v)

  }

  /**
   * Get the value change duration for this valuation.
   *
   * @returns {Number}
   */
  getValueChangeDuration() {

    return valueChangeDuration.get(this)

  }

  /**
   * Set the value change amount for this valuation.
   *
   * @param {ZillowMoney} v - The value change amount of this valuation.
   *
   * @returns {Void}
   */
  setValueChangeAmount(v) {

    valueChangeAmount.set(this, v)

  }

  /**
   * Get the value change amount for this valuation.
   *
   * @returns {ZillowMoney}
   */
  getValueChangeAmount() {

    return valueChangeAmount.get(this)

  }

  /**
   * Set the valuation low range for this valuation.
   *
   * @param {ZillowMoney} v - The valuation low range of this valuation.
   *
   * @returns {Void}
   */
  setValuationRangeLow(v) {

    valuationRangeLow.set(this, v)

  }

  /**
   * Get the valuation low range for this valuation.
   *
   * @returns {ZillowMoney}
   */
  getValuationRangeLow() {

    return valuationRangeLow.get(this)

  }

  /**
   * Set the valuation high range for this valuation.
   *
   * @param {ZillowMoney} v - The valuation high range of this valuation.
   *
   * @returns {Void}
   */
  setValuationRangeHigh(v) {

    valuationRangeHigh.set(this, v)

  }

  /**
   * Get the valuation high range for this valuation.
   *
   * @returns {ZillowMoney}
   */
  getValuationRangeHigh() {

    return valuationRangeHigh.get(this)

  }

  /**
   * Set the percentile for this valuation.
   *
   * @param {Number} p - The percentile of this valuation.
   *
   * @returns {Void}
   */
  setPercentile(p) {

    percentile.set(this, p)

  }

  /**
   * Get the percentile for this valuation.
   *
   * @returns {String}
   */
  getPercentile() {

    return percentile.get(this)

  }

  /**
   * Get the calculated percentile for this valuation.
   *
   * @returns {Number}
   */
  getReaPercentile() {

    return (this.getValuationRangeLow().getAmount() / this.getValuationRangeHigh().getAmount()) * 100

  }

  /**
   * Get the object representation for this valuation.
   *
   * @returns {Object}
   */
  toObject(format = false) {

    return {
      amount: format ? this.getAmount().getMoneyFormat() : this.getAmount().getAmount(),
      lastUpdated: this.getLastUpdated(),
      valueChange: {
        duration: this.getValueChangeDuration(),
        amount: format ? this.getValueChangeAmount().getChangeFormat() : this.getValueChangeAmount().getAmount()
      },
      valuationRange: {
        low: format ? this.getValuationRangeLow().getMoneyFormat() : this.getValuationRangeLow().getAmount(),
        high: format ? this.getValuationRangeHigh().getMoneyFormat() : this.getValuationRangeHigh().getAmount()
      },
      realPercentile: this.getReaPercentile(),
      percentile: this.getPercentile()
    }

  }

}

module.exports = ZillowValuation