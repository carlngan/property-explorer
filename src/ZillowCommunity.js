'use strict'

/**
 * The name of the community.
 * @type {String}
 * @private
 */
const name = new WeakMap()
/**
 * The id of the community.
 * @type {Number}
 * @private
 */
const id = new WeakMap()
/**
 * The type of the community.
 * @type {String}
 * @private
 */
const type = new WeakMap()
/**
 * The z index value of the community.
 * @type {String}
 * @private
 */
const zindexValue = new WeakMap()
/**
 * The overview link of the community.
 * @type {String}
 * @private
 */
const overview = new WeakMap()
/**
 * The for sale by owner link of the community.
 * @type {String}
 * @private
 */
const forSaleByOwner = new WeakMap()
/**
 * The for sale link of the community.
 * @type {String}
 * @private
 */
const forSale = new WeakMap()

/**
 * Class representing a Zillow Community.
 */
class ZillowCommunity {

  /**
   * @constructor
   *
   * @param {Object} args - Argument passed in as object.
   * @param {String} args.name - The name of the community.
   * @param {Number} args.id - The id of the community.
   * @param {String} args.type - The type of the community.
   * @param {String} args.zindexValue - The z index value of the community.
   * @param {Object} args.links - The link object.
   * @param {String} args.links.overview - he overview link of the community.
   * @param {String} args.links.forSaleByOwner - The for sale by owner link of the community.
   * @param {String} args.links.forSale - The for sale link of the community.
   *
   * @returns {Void}
   */
  constructor(args = {}) {

    if (args.name) {
      this.setName(args.name)
    }
    if (args.id) {
      this.setId(args.id)
    }
    if (args.type) {
      this.setType(args.type)
    }
    if (args.zindexValue) {
      this.setZIndexValue(args.zindexValue)
    }
    if (args.links) {
      if (args.links.overview) {
        this.setOverview(args.links.overview)
      }
      if (args.links.forSaleByOwner) {
        this.setForSaleByOwner(args.links.forSaleByOwner)
      }
      if (args.links.forSale) {
        this.setForSale(args.links.forSale)
      }
    }

  }

  /**
   * Set the name for this community.
   *
   * @param {String} n - The name of the community.
   *
   * @returns {Void}
   */
  setName(n) {

    name.set(this, n)

  }

  /**
   * Get the name for this community.
   *
   * @returns {String}
   */
  getName() {

    return name.get(this)

  }

  /**
   * Set the id for this community.
   *
   * @param {Number} i - The id of the community.
   *
   * @returns {Void}
   */
  setId(i) {

    id.set(this, i)

  }

  /**
   * Get the id for this community.
   *
   * @returns {Number}
   */
  getId() {

    return id.get(this)

  }

  /**
   * Set the status for this community.
   *
   * @param {String} t - The type of the community.
   *
   * @returns {Void}
   */
  setType(t) {

    type.set(this, t)

  }

  /**
   * Get the type for this community.
   *
   * @returns {String}
   */
  getType() {

    return type.get(this)

  }

  /**
   * Set the z index value for this community.
   *
   * @param {String} z - The z index value of the community.
   *
   * @returns {Void}
   */
  setZIndexValue(z) {

    zindexValue.set(this, z)

  }

  /**
   * Get the z index value for this community.
   *
   * @returns {String}
   */
  getZIndexValue() {

    return zindexValue.get(this)

  }

  /**
   * Set the over link for this community.
   *
   * @param {String} o - The overview link for this community.
   *
   * @returns {Void}
   */
  setOverview(o) {

    overview.set(this, o)

  }

  /**
   * Get the overview link for this community.
   *
   * @returns {String}
   */
  getOverview() {

    return overview.get(this)

  }

  /**
   * Set the for sale by owner link for this community.
   *
   * @param {String} f - The for sale by owner link for this community.
   *
   * @returns {Void}
   */
  setForSaleByOwner(f) {

    forSaleByOwner.set(this, f)

  }

  /**
   * Get teh for sale by owner for this community.
   *
   * @returns {String}
   */
  getForSaleByOwner() {

    return forSaleByOwner.get(this)

  }

  /**
   * Set the for sale link for this community.
   *
   * @param {String} f - The for sale link for this community.
   *
   * @returns {Void}
   */
  setForSale(f) {

    forSale.set(this, f)

  }

  /**
   * Get the for sale link for this community.
   *
   * @returns {String}
   */
  getForSale() {

    return forSale.get(this)

  }

  /**
   * Get the object representation for this community.
   *
   * @returns {Object}
   */
  toObject() {

    return {
      name: this.getName(),
      id: this.getId(),
      type: this.getType(),
      zindexValue: this.getZIndexValue(),
      links: {
        overview: this.getOverview(),
        forSaleByOwner: this.getForSaleByOwner(),
        forSale: this.getForSale()
      }
    }

  }

}

module.exports = ZillowCommunity