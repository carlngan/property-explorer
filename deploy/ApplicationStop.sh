#!/usr/bin/env bash
if pgrep "PM2" > /dev/null
then
    pm2 stop all
fi