#!/usr/bin/env bash
cd /home/ec2-user/app
pm2 startup redhat
sudo su -c "env PATH=$PATH:/usr/bin pm2 startup redhat -u ec2-user --hp /home/ec2-user"
pm2 start "/usr/bin/npm" -- start
pm2 save