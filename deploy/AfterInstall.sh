#!/usr/bin/env bash
sudo chown -R ec2-user /home/ec2-user/app
cd /home/ec2-user/app
yarn install
./node_modules/.bin/bower install