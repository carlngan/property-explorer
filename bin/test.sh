#!/bin/bash

TESTDIR="test"

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    unit)
        TESTDIR="test/unit"
        shift # past argument
    ;;
    integration)
        TESTDIR="test/integration"
        shift # past argument
    ;;
    *)

    ;;
esac
shift # past argument or value
done

./node_modules/.bin/mocha --recursive ${TESTDIR}