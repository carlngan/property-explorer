angular.module("propertyExplorer",
  [
    "ui.router",
    "oc.lazyLoad",
    "ngAria",
    "ngMaterial"
  ])
  .config(function ($stateProvider, $locationProvider, $httpProvider) {

    if (!$httpProvider.defaults.headers.get) {
      $httpProvider.defaults.headers.get = {};
    }
    // Disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // Extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    $stateProvider
      .state('/', {
        url: "/",
        templateUrl: "/template/search",
        controller: "SearchCtrl",
        resolve: {
          prop: function ($ocLazyLoad) {
            return $ocLazyLoad.load(
              {
                files: ["/js/SearchCtrl.js"]
              }
            );
          }
        }
      })
      .state('overview', {
        url: "/overview?address&city&state&zip&country&rentzestimate",
        templateUrl: "/template/overview",
        controller: "OverviewCtrl",
        resolve: {
          prop: function ($ocLazyLoad) {
            return $ocLazyLoad.load(
              {
                files: ["/js/OverviewCtrl.js"]
              }
            );
          }
        }
      });

    $locationProvider.html5Mode(true);

  })
  .constant("$MD_THEME_CSS", "");