angular.module("propertyExplorer")

  /**
   * PropertyExplorerService.
   */
  .service('PropertyExplorerService', [
    '$http', '$q',
    function ($http, $q) {
      return {
        searchProperty: function (dataObj) {
          var req = {
            method: 'POST',
            url: '/api/search',
            data: dataObj
          };
          return this.apiCall(req);
        },
        findAddresses: function (dataObj) {
          var req = {
            method: 'POST',
            url: '/api/address',
            data: dataObj
          };
          return this.apiCall(req);
        },
        contact: function (dataObj) {
          var req = {
            method: 'POST',
            url: '/api/contact',
            data: dataObj
          };
          return this.apiCall(req);
        },
        apiCall: function (req) {
          var deferred = $q.defer();
          $http(req)
            .then(function successCallback(data) {
              deferred.resolve(data.data);
            }, function errorCallback(data, status) {
              deferred.reject(data, status);
            })
            .catch(function (data) {
              deferred.reject(data);
            });
          return deferred.promise;
        }
      };
    }]
  );