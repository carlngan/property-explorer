angular.module("propertyExplorer")

  /**
   * PropertyExplorerCtrl.
   */
  .controller("PropertyExplorerCtrl", ["$scope",
    function ($scope) {

      /**
       * Scope variables.
       */
      $scope.buyText = "Homes for Sale";
      $scope.homesForSaleLink = "https://www.zillow.com/homes/for_sale/";
      $scope.homesForSaleByOwnerLink = "https://www.zillow.com/homes/fsbo/";

      /**
       * Setter functions.
       */
      $scope.setBuyText = function (text) {
        $scope.buyText = text;
      };
      $scope.setHomesForSaleLink = function (link) {
        $scope.homesForSaleLink = link;
      };
      $scope.setHomesForSaleByOwnerLink = function (link) {
        $scope.homesForSaleByOwnerLink = link;
      };

    }]
  );