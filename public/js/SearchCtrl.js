angular.module("propertyExplorer")

  /**
   * SearchCtrl Extends PropertyExplorerCtrl.
   */
  .controller("SearchCtrl", ["$scope", "PropertyExplorerService", "$state", "$q",
    function ($scope, PropertyExplorerService, $state, $q) {

      /**
       * Scope variables.
       */
      $scope.isDisabled    = false;
      $scope.activeRequest = null;

      /**
       * Search for addresses to populate for auto suggestions.
       * Remote PropertyExplorerService call.
       */
      $scope.querySearch = function (text) {
        if (text && text.length > 5) {
          var deferred = $q.defer();
          clearTimeout($scope.activeRequest);
          $scope.activeRequest = setTimeout(function() {
            PropertyExplorerService.findAddresses({
              text: text
            })
              .then(
                function (data) {
                  deferred.resolve(data);
                }, function (data) {
                  deferred.reject(data);
                }
              )
              .catch(function (data) {
                deferred.reject(data);
              });
          }, 1000);
          return deferred.promise;
        }
        else {
          return [];
        }
      };

      /**
       * Perform a search on selected address.
       */
      $scope.search = function () {

        $scope.searchError = false;

        //if user is submitting a blank search
        if (!$scope.searchText) {
          return $scope.searchError = "A full address is required."
        }
        //check to make sure address is good
        if($scope.selectedItem && $scope.selectedItem.address_components) {

          var addressComponents = $scope.selectedItem.address_components;

          var street_number = null;
          var route = null;
          //optional apt, suite, etc.
          var subpremise = null;
          //city
          var locality = null;
          //state
          var administrative_area_level_1 = null;
          //zip
          var postal_code = null;

          for(var i = 0; i < addressComponents.length; i++) {
            var component = addressComponents[i];
            if (component.types.indexOf("street_number") > -1) {
              street_number = component.short_name;
            }
            else if (component.types.indexOf("route") > -1) {
              route = component.short_name;
            }
            else if (component.types.indexOf("subpremise") > -1) {
              subpremise = component.short_name;
            }
            else if (component.types.indexOf("locality") > -1) {
              locality = component.short_name;
            }
            else if (component.types.indexOf("administrative_area_level_1") > -1) {
              administrative_area_level_1 = component.short_name;
            }
            else if (component.types.indexOf("postal_code") > -1) {
              postal_code = component.short_name;
            }
          }

          if (street_number && route && locality && administrative_area_level_1 && postal_code) {
            var formattedAddress = street_number + " " + route;
            if (subpremise) {
              formattedAddress = formattedAddress + " #" + subpremise;
            }

            return $state.go('overview', {
              address: formattedAddress,
              city: locality,
              state: administrative_area_level_1,
              zip: postal_code,
              rentzestimate: $scope.rentzestimate
            })
          }

        }

        $scope.searchError = "Unable to parse your address, please try again or select from the suggested addresses.";

      };

      /**
       * Functions not used now -- but may be useful
       */
      $scope.searchTextChange = function(text) {
        return;
      };

      $scope.selectedItemChange = function (item) {
        return;
      };

    }]
  );