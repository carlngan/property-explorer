angular.module("propertyExplorer")

  /**
   * OverviewCtrl Extends PropertyExplorerCtrl.
   */
  .controller("OverviewCtrl", ["$scope", "PropertyExplorerService", "$state", "$rootScope", "$stateParams",
    function ($scope, PropertyExplorerService, $state, $rootScope, $stateParams) {

      /**
       * Scope variables.
       */
      $scope.message = {};
      $scope.result = {};
      $scope.zestimateTooltip = "A Zestimate home valuation is Zillow's estimated market value. It is not an appraisal. " +
        "Use it as a starting point to determine a home's value. " +
        "The Value Range is the high and low estimate market value for which Zillow values a home. The more information, " +
        "the smaller the range, and the more accurate the Zestimate.";
      $scope.rentZestimateTooltip = "A Rent Zestimate is Zillow's estimated monthly rental price, computed using a " +
        "proprietary formula. It is a starting point in determining the monthly rental price for a specific property. " +
        "The Rent Range is the high and low estimate for which an apartment or home could rent. The more information we " +
        "have, the smaller the range, and the more accurate the Rent Zestimate.";
      $scope.medianTooltip = "The Zillow Home Value Index is the median Zestimate valuation for a given geographic area on a given day.";
      $scope.contactField = {
        message: "Hi Carl, I am impressed with what you are doing and would like to offer you an opportunity at Zillow :)"
      };

      /**
       * Check if a formatted value is positive or not.
       */
      $scope.valueIncreased = function(formattedValue) {
        return formattedValue[0] === "+"
      };

      /**
       * Lookup the piece of property requested.
       */
      PropertyExplorerService.searchProperty({
        address: $stateParams.address,
        city: $stateParams.city,
        state: $stateParams.state,
        zip: $stateParams.zip,
        rentzestimate: $stateParams.rentzestimate
      })
        .then(
          function(data) {

            if (data.message) {
              $scope.message.code = data.message.code;
              $scope.message.resolution = data.message.resolution;
              $scope.message.text = data.message.text;
            }
            if (data.result) {
              $scope.result.address = data.result.address;
              $scope.result.links = data.result.links;
              $scope.result.localRealEstate = data.result.localRealEstate;
              $scope.result.rentzestimate = data.result.rentzestimate;
              $scope.result.zestimate = data.result.zestimate;
              $scope.result.zpid = data.result.zpid;

              //Set menu links
              if ($scope.result.localRealEstate.name) {
                $scope.setBuyText($scope.result.localRealEstate.name + " homes for Sale")
                if ($scope.result.localRealEstate.links.forSale) {
                  $scope.setHomesForSaleLink($scope.result.localRealEstate.links.forSale)
                }
                if ($scope.result.localRealEstate.links.forSaleByOwner) {
                  $scope.setHomesForSaleByOwnerLink($scope.result.localRealEstate.links.forSaleByOwner)
                }
              }
            }
            console.log("data", data)
          }, function(data) {
            console.log("ERROR", data);
          }
        )
        .catch(function(data){
          console.log("CATCH", data);
        });

      /**
       * Contact Carl.
       */
      $scope.contact = function() {

        $scope.contactError = null;
        $scope.contactSuccessful = false;

        if (!$scope.contactField.name){
          return $scope.contactError = "Name is required.";
        }
        if (!$scope.contactField.phone){
          return $scope.contactError = "Phone is required.";
        }
        if (!$scope.contactField.email){
          return $scope.contactError = "E-mail is required.";
        }
        if (!$scope.contactField.message){
          return $scope.contactError = "Message is required.";
        }

        PropertyExplorerService.contact({
          name: $scope.contactField.name,
          phone: $scope.contactField.phone,
          email: $scope.contactField.email,
          message: $scope.contactField.message,
          serious: $scope.contactField.serious
        })
        .then(
          function() {
            $scope.contactSuccessful = true;

          }, function(data) {
            $scope.contactError = data.msg;
          }
        )
          .catch(function(data){
            $scope.contactError = data.msg;
          });
      }

    }]
  );